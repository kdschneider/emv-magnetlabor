---
title: "EMV Magnetlabor"
author: "Konstantin Schneider"
date: "`r format(lubridate::today(), '%d. %b %Y')`"
site: bookdown::bookdown_site
url: https://kdschneider.gitlab.io/emv-magnetlabor
cover-image: static/img/title_image.jpg
description: Bericht zur EMV des Magnetlabors.
bibliography: static/bib.bib
favicon: static/img/favicon.ico
# latex
## eisvogel
titlepage: yes
titlepage-background: static/titlepage_background.pdf
titlepage-text-color: FFFFFF
titlepage-rule-height: 0
toc-own-page: yes
footnotes-pretty: yes
## pandoc
link-citations: yes
link-bibliography: yes
pandoc_args: [--csl=static/csl.csl] 
---

`r if (knitr::is_latex_output()) '<!--'`

# Index {.unlisted .unnumbered #index}

```{r}
library(tidyverse)
```

### PDF {-}

[Klick](./emv-magnetlabor.pdf)

### Quellcode {-}

Der Quellcode dieses Berichts steht [hier](https://gitlab.com/sck57752/emv-magnetlabor) zur Verfügung. 
Die Auswertungen wurden in `R` geschrieben. 
Der Bericht selbst wurde mit dem `R` Paket `{bookdown}` erstellt. 

### Bericht lokal rendern {-}

Um den kompletten Bericht mit allen Daten etc. lokal zu erzeugen, kann das R-Script `_build.R` ausgeführt werden. 
Die benötigten Pakete können über das R-Paket `{renv}` mit dem Befehl `renv::restore()` installiert werden. 

### Session Info {-}

```{r}
sessioninfo::platform_info() |>
  enframe() |> 
  unnest(value) |> 
  pivot_wider(id_cols = everything(), names_from = "name", values_from = "value") |> 
  select(
    "Version" = version,
    "OS" = os,
    "System" = system,
    pandoc
  ) |> 
  pivot_longer(cols = everything()) |> 
  knitr::kable(col.names = c("", ""))
```

`r if (knitr::is_latex_output()) '-->'`
