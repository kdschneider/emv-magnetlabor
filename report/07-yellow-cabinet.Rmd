# Messung im Gefahrenschrank {#yellow-cabinet}

Eine weitere Möglichkeit das Rauschverhalten und die Störempfindlichkeit von Messungen zu verbessern, könnte das Verwenden eines geschlossenen, geerdeten Messschranks aus Metall sein.

Bevor ein solcher angeschafft oder gebaut wird, soll in diesem Versuch geprüft werden, ob sich die Messungen am bisherigen Aufbau verbessern, wenn diese im Gefahrenschrank im Messspitzenlabor durchgeführt werden.

TODO

```{r effect-plot, fig.cap = "TODO"}
read_rds(here::here("data/plots/yellow-cabinet-effect.rds"))
```

```{r plot-summary-1}
knitr::kable(
  x = read_rds(here::here("data/tables/yellow-cabinet-summary-1.rds")),
  caption = "Mittlere Standardabweichung des Rauschens.",
  digits = 3
)
```

```{r plot-summary-2}
knitr::kable(
  x = read_rds(here::here("data/tables/yellow-cabinet-summary-2.rds")),
  caption = "Mittlere Peak-to-Peak Amplitude eines Stoersignals.",
  digits = 3
)
```
