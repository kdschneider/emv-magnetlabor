ANALOG
Ch 1 Scale 500mV/, Pos -31.50mV, Coup DC, BW Limit Off, Inv Off, Imp 1M Ohm
     Probe 1.0000000 : 1, Skew 0.0s

TRIGGER
Sweep Mode Auto, Coup DC, Noise Rej Off, HF Rej Off, Holdoff 60.0ns
Mode Edge, Source Ch 1, Slope Rising, Level 208.50mV

HORIZONTAL
Mode Normal, Ref Center, Main Scale 50.00ms/, Main Delay 0.0s

ACQUISITION
Mode Normal, Realtime On, Vectors On, Persistence Off

MEASUREMENTS
RMS - Full Screen(1), Cur 322mV
Pk-Pk(1), Cur 2.45V

