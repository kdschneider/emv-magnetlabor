---
title: Test der neuen Kabel
subtitle: Auswertung
author: Konstantin Schneider
editor_options: 
  chunk_output_type: console
---

# Setup {-}
## Pakete {-}

```{r}
# pakete
library(DoE.base)
library(patchwork)
library(tidyverse)
library(parsnip)
library(latex2exp)
```

## Funktionen {-}

```{r}
read_scope_csv <- function(file) {
  data <-
    read_csv(file = file, skip = 1) |>
    rename(
      "seconds" = "second",
      "volts" = "Volt"
    )
    return(data)
}

create_effect_plot <- function(data, factor, response) {
  df <-
    data |> 
    as_tibble() |> 
    group_by({{ factor }}) |> 
    summarise(mean_response = mean({{ response }}))  
  
  max_response <-
    df |> 
    pull(mean_response) |> 
    max()
  
  plot <- 
    df |> 
    ggplot() +
    aes(x = {{ factor }}, y = mean_response, group = 1) +
    geom_point() +
    geom_line() +
    theme(axis.title.x = element_blank()) +
    scale_y_continuous(
      sec.axis = sec_axis(
        trans = ~ ./max_response,
        labels = scales::percent
      )
    )
  
  return(plot)
}

read_data <- function(path) {
  data <-
    tibble(
      path = fs::dir_ls(
                   path = path,
                   recurse = TRUE,
                   glob = "*.csv"
                 )
    ) |>
    mutate(
      index = fs::path_dir(path) |> 
        basename() |> 
        as.numeric()
    ) |> 
    mutate(measurement = map(path, read_scope_csv)) |> 
    unnest(measurement) |> 
    group_by(path) |> 
    mutate(
      pp = abs(max(volts) - min(volts)),
      sd = sd(volts, na.rm = TRUE)
    ) |> 
    nest(measurement = c(seconds, volts))

  return(data)
}
```

# Versuchsplan

```{r}
cables <- c(
    "RG-58",
    "RG-58 + Kupfer",
    "RG-223",
    "RG-142",
    "RG-400"
  ) |>
  as_factor()

factors <- list(
  "cable" = cables,
  "iso" = c("Ja", "Nein"),
  "ground" = c("Ja", "Nein")
)

ff_cables <-
  DoE.base::fac.design(
    factor.names = factors,
    seed = 123
  )
```

# Daten einlesen

```{r}
df_noise <-
  read_data(here::here("data-raw/cable-comparison/all/noise"))

ff_noise <-
  ff_cables |> 
  add.response(
    response = df_noise |> select(-index)
  )

write_rds(
  x = ff_noise,
  file = here::here("data/new-cables-noise.rds")
)
```

```{r}
peak_data <-
  read_data(here::here("data-raw/cable-comparison/all/peak"))

ff_peak <-
  ff_cables |> 
  add.response(
    response = peak_data |> select(-index)
  )
```

# Auswertung
## Rauschen

```{r noise-effect, fig.cap="Mittlere Standardabweichung der Faktorlevel. Das geringste Rauschen zeigt das zusätzlich mit einem Kupfergeflecht geschirmte RG-58 Kabel."}
p1 <- 
  create_effect_plot(ff_noise, cable, sd) +
  ylim(c(0.32, 0.38)) +
  labs(
    subtitle = "Kabel",
    y = TeX("$\\sigma$")
  ) 

p2 <-
  create_effect_plot(ff_noise, iso, sd) +
  ylim(c(0.32, 0.38)) +
  labs(
    subtitle = "Trenntrafo",
    y = TeX("$\\sigma$")
  ) 

p3 <-
  create_effect_plot(ff_noise, ground, sd) +
  ylim(c(0.32, 0.38)) +
  labs(
    subtitle = "Erdung",
    y = TeX("$\\sigma$")
  ) 

p.noise_effect <- p1 / (p2 + p3) & plot_annotation(title = "Mittlere Standardabweichung")

p.noise_effect

write_rds(
  x = p.noise_effect,
  file = here::here("data/plots/new-cables-noise-effect.rds")
)
```

## Peak-to-Peak

```{r pp-effect, fig.cap="Mittlere Peak-to-Peak Amplitude der Faktorlevel. Während die neuen Kabel RG-223, RG-142 und RG-400 niedrigere Werte als die vorhandenen RG-58 Kabel liefern, schneidet das RG-58 Kabel mit extra Kupfergeflecht am besten ab. Dagegen zeigen Trenntrafo und Erdung einen geringeren Effekt."}
p1 <- 
  create_effect_plot(ff_peak, cable, pp) +
  ylim(c(11, 23)) +
  labs(
    subtitle = "Kabel",
    y = "Spannung [V]"
  ) 

p2 <- 
  create_effect_plot(ff_peak, iso, pp) +
  ylim(c(11, 23)) +
  labs(
    subtitle = "Trenntrafo",
    y = "Spannung [V]"
  ) 

p3 <- 
  create_effect_plot(ff_peak, ground, pp) +
  ylim(c(11, 23)) +
  labs(
    subtitle = "Erdung",
    y = "Spannung [V]"
  ) 

p.pp_effect <- p1 / (p2 + p3) & plot_annotation(title = "Mittlere Peak-to-Peak Amplitude")

p.pp_effect

write_rds(
  x = p.pp_effect,
  file = here::here("data/plots/new-cables-peak-effect.rds")
)
```

# Kontrolle

```{r}
set.seed(123)
control <- 
  tibble(cable = rep(cables, times = 5)) |> 
  mutate(order = row_number()) |> 
  slice_sample(n = length(cables) * 5) |> 
  mutate(index = row_number()) |> 
  select(index, order, cable)
```

```{r}
df_control <-
  read_csv(here::here("data-raw/cable-comparison/cable-only/cables.csv")) |> 
  mutate(
    cable = as_factor(cable) |>
      fct_relevel(
        "RG-58",
        "RG-58 + Kupfer",
        "RG-223",
        "RG-142",
        "RG-400"
      )
  ) |>
  arrange(order)
```

## Rauschen {-}

```{r}
df_noise_control <-
  read_data(path = here::here("data-raw/cable-comparison/cable-only/noise")) |> 
  left_join(df_control, by = "index") |> 
  mutate(
    cable = as_factor(cable) |>
      fct_relevel(
        "RG-58",
        "RG-58 + Kupfer",
        "RG-223",
        "RG-142",
        "RG-400"
      )
  ) |>
  select(index, cable, sd)
```

```{r noise-control-effect, fig.cap="Mittlere Standardabweichung der unterschiedlichen Kabel. Die geringste mittlere Standardabweichung, und damit das geringste Rauschen, zeigt das RG-58 Kabel mit extra Kupfergeflecht. Die teureren RG-142 und RG-400 liefern dagegen das größte Rauschen."}
p.noise_control_effect <-
  create_effect_plot(df_noise_control, cable, sd) +
  labs(
    title = "Mittlere Standardabweichung",
    x = "Kabel",
    y = TeX("$\\sigma$")
  )

p.noise_control_effect

write_rds(
  x = p.noise_control_effect,
  file = here::here("data/plots/new-cables-noise-control-effect.rds")
)
```

```{r}
tbl.noise_control <-
  df_noise_control |> 
  group_by(cable) |> 
  summarise(sd = mean(sd)) |> 
  select(cable, sd) |> 
  arrange(sd) |> 
  knitr::kable(
    digits = 4,
    col.names = c("Kabel", "Mittlere Standardabweichung"),
    caption = "..."
  )

write_rds(
  x = tbl.noise_control,
  file = here::here("data/tables/new-cables-noise-control.rds")
)
```

## Störanfälligkeit {-}

```{r}
df_peak_control <-
  read_data(here::here("data-raw/cable-comparison/cable-only/peak")) |>
  left_join(df_control, by = "index") |> 
  select(index, cable, pp)
```

```{r peak-control-effect, fig.cap="Die Störanfälligkeit verbessert sich mit den neuen Kabeln. Es gilt: Je höher der Preis des Kabels, desto geringer die gemessene mittlere Peak-to-Peak Spannung."}
p.pp_control_effect <-
  create_effect_plot(df_peak_control, cable, pp) +
  labs(
    title = "Mittlere Peak-to-Peak Spannung",
    x = "Kabel",
    y = "Spannung [V]"
  )

p.pp_control_effect 

write_rds(
  x = p.pp_control_effect,
  file = here::here("data/plots/new-cables-peak-control-effect.rds")
)
```

```{r}
tbl.pp_control <-
  df_peak_control |> 
  group_by(cable) |> 
  summarise(pp = mean(pp)) |> 
  select(cable, pp) |> 
  arrange(pp) |>
  mutate(pp = glue::glue("{round(pp, 4)} V")) |>
  knitr::kable(
    digits = 4,
    col.names = c("Kabel", "Mittlere Peak-to-Peak Amplitude"),
    align = c("lr"),
    caption = "..."
  )

tbl.pp_control 

write_rds(
  x = tbl.pp_control,
  file = here::here("data/tables/new-cables-peak-control.rds")
)
```
