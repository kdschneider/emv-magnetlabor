---
title: Vergleich der aktuellen Kabel
subtitle: Auswertung
author: Konstantin Schneider
editor_options: 
  chunk_output_type: console
---

```{r setup}
library(tidyverse)
library(patchwork)
#library(gRaffl)
```

```{r}
read_scope <- function(file) {
  
  data <-
    readr::read_csv(
      file = file,
      skip = 1
    ) |>
    dplyr::rename("seconds" = "second", "volts" = "Volt")
  
  return(data)
  
}

df_cc <-
  tibble(filename = list.files(here::here("data-raw/current-cables"), pattern = "*.csv")) |>
  filter(filename != "index.csv") |>
  mutate(
    measurement = map(
      .x = filename,
      .f = function(x) { read_scope(here::here("data-raw/current-cables", x)) }
    )
  ) |>
  full_join(read_csv(here::here("data-raw/current-cables/index.csv"))) |>
  group_by(length, iso_traf, ground, extra_ins) |>
  mutate(rep = row_number()) |>
  ungroup() |>
  mutate(
    across(length:extra_ins, forcats::as_factor),
    
    length = fct_relevel(length, "1", "2", "3") |>
      fct_recode("1m" = "1", "2m" = "2", "3m" = "3"),
    
    iso_traf = 
      fct_relevel(iso_traf, "TRUE", "FALSE") |>
      fct_recode("Ja" = "TRUE", "Nein" = "FALSE"),
    
    ground = 
      fct_relevel(ground, "TRUE", "FALSE") |>
      fct_recode("Ja" = "TRUE", "Nein" = "FALSE"),
    
    extra_ins = 
      fct_relevel(extra_ins, "FALSE", "single", "double") |>
      fct_recode(
        "ohne" = "FALSE",
        "einfach" = "single",
        "doppelt" = "double"
      )
  ) |> 
  unnest(measurement)

write_rds(
  x = df_cc,
  file = here::here("data/current-cables_data.rds")
)
```

### Experimental Design

```{r doe-table}
t_cc_doe <-
  df_cc |>
  select(length:extra_ins) |>
  unique() |>
  arrange(length) |>
  knitr::kable(
    col.names = c(
      "Länge",
      "Trenntrafo",
      "Erdung",
      "Kupfergeflecht"
    ),
    caption = "Gemessene Kombinationen der Faktorlevel. Kabel mit Länge zwei und drei Meter konnten nur ohne äußeres Kupfergeflecht gemessen werden."
  )

write_rds(
  x = t_cc_doe,
  file = here::here("data/tables/current-cables_doe.rds")
)
```

## Ergebnisse

```{r}
p_cc_measurement <-
  df_cc |>
  drop_na() |>
  filter(
    length == "2m",
    iso_traf == "Ja",
    ground == "Nein",
    extra_ins == "ohne"
  ) |>
  ggplot() +
  aes(
    x = seconds * 1000,
    y = volts,
    colour = forcats::as_factor(rep),
    group = filename
  ) +
  geom_line() +
  labs(
    title = "Störsignal",
    subtitle = "durch Einschalten der Raumbeleuchtung.",
    x = "Zeit [ms]",
    y = "Spannung [V]"
  ) +
  xlim(-0.1, 0.2) +
  theme(legend.position = "none")

p_cc_measurement

write_rds(
  x = p_cc_measurement,
  file = here::here("data/plots/current-cables_measurement-example.rds")
)
```

```{r} 
create_pp_plot <- function(data, x) {
  
  df <- 
    {{ data }} |> 
    group_by(filename, {{ x }}) |> 
    summarise(pp = max(volts) - min(volts)) |> 
    group_by({{ x }}) |> 
    summarise(pp = mean(pp)) 
  
  max_response <- 
    df |> 
    pull(pp) |> 
    max()
  
  p <-
    df |> 
    ggplot() +
    aes(
      x = {{ x }},
      y = pp,
      group = 1
    ) +
    geom_line() +
    geom_point() +
    theme(axis.title.x = element_blank()) +
    labs(y = "Spannung [V]") +
    scale_y_continuous(
      sec.axis = sec_axis(
        trans = ~ ./max_response,
        labels = scales::percent
      )
    ) +
    ylim(0, 17.5)
  
  return(p)
  
}

p1 <-
  df_cc |> 
  filter(extra_ins == "ohne") |> 
  create_pp_plot(length) +
  labs(subtitle = "Kabellänge (ohne Kupfergeflecht)")

p2 <-
  df_cc |> 
  filter(length == "1m") |> 
  create_pp_plot(extra_ins) +
  theme(axis.title.y = element_blank()) +
  labs(subtitle = "Extra Schirmung (1m Kabel)")

p3 <- 
  df_cc |> 
  create_pp_plot(ground) +
  labs(subtitle = "Erdung")

p4 <- 
  df_cc |> 
  create_pp_plot(iso_traf) +
  theme(axis.title.y = element_blank()) +
  labs(subtitle = "Trenntrafo")

p_cc_pp <- 
  (p1 + p2)/(p3 + p4) &
  plot_annotation(title = "Mittlere Peak-to-Peak-Amplitude")

p_cc_pp

write_rds(
  x = p_cc_pp,
  file = here::here("data/plots/current-cables_peak-to-peak.rds")
)
```

